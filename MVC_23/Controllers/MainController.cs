﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_23.Controllers
{
    public class MainController : Controller
    {
        public ActionResult MainPage()
        {
            return View(HttpContext.Application["articles"]);
        }
    }
}