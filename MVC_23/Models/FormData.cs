﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_23.Models
{
    public class FormData
    {
        public string Visit { get; set; }
        public string Satisfaction { get; set; }
        public string PrimaryUsing { get; set; }
        public string LevelOfService { get; set; }

    }
}